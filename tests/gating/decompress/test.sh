rm -rf in out exp
echo "Hello World" > in
echo "Hello World" > exp

zstd ./in -o ./out

rm in

zstd -d ./out -o ./in

diff ./in ./exp || exit 1  

rm -rf in out exp

exit 0
